<!DOCTYPE html>
<html lang="en">
<head>
    <title>Something</title>
    <meta charset="UTF-8">
    <style>
        body {
            margin: 0;
            padding: 10px;
        }

        .grid {
            border: 1px solid #333;
            width: 300px;
        }

        .grid > div {
            display: flex;
        }

        .grid i {
            display: inline-block;
            height: 30px;
            width: 10%;
        }

        .grid .is_active {
            background: #333;
        }
    </style>
</head>
<body>
<?php
$randomX = NumberGenerator::getRandom();
$randomY = NumberGenerator::getRandom();
?>
<div class="grid">
    <?php for ($row = 1; $row <= 10; $row++) { ?>
        <div>
            <?php for ($column = 1; $column <= 10; $column++) { ?>
                <?php $isActive = $randomX === $row && $randomY === $column; ?>
                <i<?= $isActive ? ' class="is_active"' : '' ?>></i>
            <?php } ?>
        </div>
    <?php } ?>
</div>
</body>
</html>
<?php

class NumberGenerator
{
    const MIN_NUMBER = 1;
    const MAX_NUMBER = 10;

    public static function getRandom()
    {
        return rand(self::MIN_NUMBER, self::MAX_NUMBER);
    }
}
